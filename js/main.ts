let canvas = document.querySelector('canvas');

canvas.width = 1200; //window.innerWidth;
canvas.height = 500; //window.innerHeight;

let c = canvas.getContext('2d');

declare const circleDefaults : {
    radius: 10
};

let circleArray = [];

class circle {
    x: number;
    y: number;
    radius: number;
    dx: number;
    dy: number;
    color: string;

    constructor(x: number, y:number, radius:number, dx: number, dy: number){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.dx = dx;
        this.dy = dy;
        this.color = 'green';
    }
} //end circle class

let i = 0;
for (i=0; i <= 20; i++){
    let x = (Math.random() * 200) + 30;
    let y = Math.random() * 200 + 30; 
    let radius = Math.random() * 10;
    let dx = Math.random() * 20;
    let dy = Math.random() * 20;


    let newCircle = new circle(x, y, radius, dx, dy);

    circleArray.push(newCircle);

}

function animate(){
    requestAnimationFrame(animate);
    c.clearRect(0, 0, canvas.width, canvas.height )
    let i = 0;

    
    
    for (i = 0; i < circleArray.length; i++){
        console.log(circleArray[i]);

        c.beginPath();
        c.arc(circleArray[i].x, circleArray[i].y, circleArray[i].radius, 0, Math.PI * 2, false);
        c.strokeStyle = 'green';
        

        if (circleArray[i].x > canvas.width - circleArray[i].radius){
            circleArray[i].dx = -circleArray[i].dx;
        }
        if (circleArray[i].x <= 0 + circleArray[i].radius){
            circleArray[i].dx = -circleArray[i].dx;
        }
        
        if (circleArray[i].y > canvas.height - circleArray[i].radius){

            circleArray[i].dy = -circleArray[i].dy;
        }
        if (circleArray[i].y <= 0 + circleArray[i].radius){
            circleArray[i].dy = -circleArray[i].dy;
        }
        circleArray[i].x += circleArray[i].dx;
        circleArray[i].y += circleArray[i].dy;
        c.stroke();
    }
    
    
}

animate();