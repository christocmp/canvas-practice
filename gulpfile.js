// Sass configuration
var gulp = require('gulp');
var sass = require('gulp-sass');

var paths = {
    styles: {
        src: './scss/*',
        loc: './scss/**/*',
        dest: './css'
    }
}

gulp.task('sass', function() {
    gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(gulp.dest(paths.styles.dest))
});

gulp.task('default', ['sass'], function() {
    gulp.watch(paths.styles.loc, ['sass']);    
})